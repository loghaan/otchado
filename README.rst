===========================
 ``otchado`` Documentation
===========================
----------------
 ``README.rst``
----------------

.. _docker: http://docker.com

.. _qemu: http://qemu.org

.. _python: http://python.org

.. _RaspberryPi: https://www.raspberrypi.org

.. _raspios: https://www.raspberrypi.org/downloads/raspberry-pi-os/

.. _fuse: https://github.com/libfuse/libfuse

.. _yocto: https://www.yoctoproject.org/

.. _buildroot: https://buildroot.org/


Introduction
============

``otchado`` is a tool to generate disk images for the RaspberryPi_ platform.
The tool is based on docker_, qemu_ and python_. Generated disk images are
derived from a raspios_ distribution.

In a sense, service provided by ``otchado`` can be compared to service provided
by software like yocto_ or buildroot_. Those latter are able to generate
a complete linux distribution from scratch for many platforms using cross compilation
but they can also be complicated to set up. Sometimes, add or remove some packages
by simply using the package manager and modify some configuration files is often
easier than creating your own linux distribution. The main idea of ``otchado`` is
exactly to start from a well-known distro and customize it via the docker_ machinery.
Building from source with ``otchado`` is still possible but it will give yourself
a little more trouble. Standard tool-chain of the distro (emulated with ``qemu``) or cross
tool-chains can both be setup in the same way as in a standard ``Dockerfile``.

Note that this project is currently at alpha stage. Only RaspberryPi_ platform
and raspios_ distro is supported. But improvements with further distributions and/or platforms
could be done in the future.


Requirements
============

``otchado`` requires some packages to be installed on your operating system. On a ``debian``
OS they can be installed like this:


.. code-block:: shell-session

    # apt-get install docker.io python3 python3-venv python3-pip qemu-user-static

It also requires that your linux kernel supports fuse_ (``CONFIG_FUSE_FS``).


Create python virtual environment
=================================

A python virtual environment must also be created:

.. code-block:: shell-session

    $ python3.9 -m venv --prompt otchado venv
    $ ./shell
    (otchado) $ pip install [-e] .


Get shell with ``otchado`` environment
======================================

Get a shell ready to start working with ``otchado``. The script ``./shell``
loads the virtual environment and sources the file ``./env``.

.. code-block:: shell-session

    $ ./shell
    (otchado) $


User interface
==============

``otchado`` comes with a command line tool of the same name which control
the launch of disk images generation. It also provides some recipes which
programmatically describes operations to build such or such images. Each recipe
is composed of tasks and subtasks which can be launched individually.


Command line usage
~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    (otchado) $ otchado -h
    usage: otchado <init|show-config|show-recipes|show-tasks|run> [optional arguments]

    optional arguments:
      -h, --help         show this help message and exit
      --log-level LEVEL  sset log level to LEVEL
      --no-docker-cache  disable docker cache
      --reset            reset recipe build dir
      -v                 set log level to 'info2'
      -q                 set log level to 'error'


Create work directory
~~~~~~~~~~~~~~~~~~~~~

First ``.otchado/`` work dir must be created. This is done by typing:

.. code-block:: shell-session

    (otchado) $ otchado init

``.otchado/`` is created in the current directory.


List ``otchado`` recipes
~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    (otchado) $ otchado show-recipes
    * recipes list
    :debootstrap
    :disktools
    :pidemo-hdmisrc
    :pidemo-motd
    :raspios


Inspect tasks and subtasks included in ``raspios`` recipe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    (otchado) $ otchado -q show-tasks :raspios
    * tasks tree of recipe :raspios
    :raspios                         <otchado.task.RootTask>
    ├── :preprocessing               <otchado.tasks.template.Processor>
    ├── :bootstrap                   <otchado.tasks.raspios.Bootstrap>
    │   ├── :stage1                  <otchado.task.MethodTask>
    │   └── :stage2                  <otchado.tasks.docker.Build>
    └── :pack                        <otchado.tasks.raspios.Pack>
        ├── :mktars                  <otchado.task.MethodTask>
        ├── :gendisk                 <otchado.task.MethodTask>
        └── :keepdisk                <otchado.task.MethodTask>


Execute a task
~~~~~~~~~~~~~~

.. code-block:: shell-session

    (otchado) $ otchado -v run <task-name>


Functional description of the ``raspios`` recipe
================================================

Disk generation for the RaspberryPi_ is assumed by the ``raspios`` recipe
which describes the build instructions. The entry point of the ``raspios`` recipe
is located in ``lib/python/otchado/recipes/distros/raspios/raspios.otr`` .
Currently the recipe provides three main tasks: ``preprocessing``, ``bootstrap``
and ``pack``

``preprocessing``
    preprocess some files included in recipe (usually with extension ``*.jinja2``) from
    settings defined in ``raspios.otr``

``bootstrap``
    fetch minimal ``raspios`` packages from official repositories using ``debootstrap``
    and make a docker image called ``otchado-raspios``

``pack``
    convert ``otchado-raspios`` docker image to an image disk ``raspios.disk`` wich
    can be burn on a sdcard


The diagram below summarizes the work-flow of the recipe:


.. uml::

    @startuml
    skinparam rectangle<<**file**>> {
      backgroundColor LightBlue
    }
    skinparam rectangle<<**debian repository**>> {
      backgroundColor LightGreen
    }
    skinparam rectangle<<**docker image**>> {
      backgroundColor Pink
    }

    rectangle "raspios.otr" as rotr<<**file**>>
    rectangle "Dockerfile-bootstrap-stage2.jinja2" as dbst2<<**file**>>
    rectangle "Dockerfile-bootstrap-stage2" as dbst<<**file**>>
    rectangle "sources.list.jinja2" as srcl2<<**file**>>
    rectangle "sources.list" as srcl<<**file**>>

    rotr --> (preprocessing)
    dbst2 --> (preprocessing)
    srcl2 --> (preprocessing)
    (preprocessing) --> dbst
    (preprocessing) --> srcl

    rectangle "http://raspbian.raspberrypi.org/raspbian/" as http<<**debian repository**>>
    rectangle "raspios.disk" as disk<<**file**>>
    rectangle "otchado-raspios" as img<<**docker image**>>

    rotr --> (bootstrap)
    dbst  --> (bootstrap)
    srcl  --> (bootstrap)
    http --> (bootstrap)
    (bootstrap) --> img

    rotr --> (pack)
    img --> (pack)
    (pack) --> disk

    @enduml


Playing with ``raspios`` recipe
===============================

Build the intermediate docker images ``:debootstrap`` and ``:disktools``.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The ``raspios`` recipe uses internally some native external tools
``(debootstrap, fdisk, mtools, parted, libfuse, …)``. Those tools are provided
under the form of a docker images. This required intermediate docker images,
called ``otchado-debootstrap`` and ``otchado-disktools``, can be built by running the associated
recipe ``:debootstrap`` and ``:disktools``.

.. code-block:: shell-session

    (otchado) $ otchado run :debootstrap :disktools -v
    …
    (otchado) $ docker image ls
    REPOSITORY          TAG     IMAGE ID      CREATED        SIZE
    …
    otchado-disktools   latest  675a55ef8937  2 seconds ago  332MB
    otchado-debootstrap latest  a2df431c3826  2 seconds ago  121MB
    …


Preprocess recipe files
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    (otchado) $ otchado run :raspios:preprocessing -v
    …


Make docker image from reference ``raspios`` distro
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    (otchado) $ otchado run :raspios:bootstrap -v
    …
    (otchado) $ docker image ls
    REPOSITORY          TAG      IMAGE ID      CREATED             SIZE
    …
    otchado-raspios     latest   22028c3ca31f  About a minute ago  1.09GB
    …


Check that the generated docker image is *really* a ``raspios`` system
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    (otchado) $ docker run -it otchado-raspios /bin/bash
    root@23189f9851ec:/# file /bin/ls
    /bin/ls: ELF 32-bit LSB executable, ARM, EABI5 version 1 (SYSV), dynamically linked,…
    root@23189f9851ec:/# exit


Perform "manual" customizations directly with docker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    (otchado) $ docker run -it otchado-raspios /bin/bash


Modify message of the day
-------------------------

.. code-block:: shell-session

    root@79b7204ccba0:/# date +"=== Hello world / %H:%M:%S ===" > /etc/motd
    root@79b7204ccba0:/# cat /etc/motd
    === Hello world / 15:49:37 ===


Enable ``ssh`` service
----------------------

.. code-block:: shell-session

    root@79b7204ccba0:/# systemctl enable ssh
    Synchronizing state of ssh.service with SysV service script with /lib/systemd/…
    Executing: /lib/systemd/systemd-sysv-install enable ssh
    Created symlink /etc/systemd/system/sshd.service → /lib/systemd/system/ssh.service.
    Created symlink /etc/systemd/system/multi-user.target.wants/ssh.service → /lib/systemd/…
    root@79b7204ccba0:/# echo "PermitRootLogin yes" >> /etc/ssh/sshd_config


Set ``root`` password
---------------------

.. code-block:: shell-session

    root@79b7204ccba0:/# passwd
    Enter new UNIX password:
    Retype new UNIX password:
    root@79b7204ccba0:/#


Commit container changes and check image
----------------------------------------

.. code-block:: shell-session

    root@79b7204ccba0:/# exit
    (otchado) $ docker ps -a
    CONTAINER ID  IMAGE            COMMAND      CREATED         STATUS
    c44c9dc9988f  otchado-raspios  "/bin/bash"  3 seconds ago   Exited (0)
    (otchado) $ docker commit c44c9dc9988f otchado-raspios
    …
    (otchado) $ docker run -it otchado-raspios cat /etc/motd
    === Hello world / 15:49:37 ===


Generate disk from modified docker image
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    (otchado) $ otchado run :raspios:pack -v

This create the disk image ``.otchado/build/precious/raspios.disk``


Burn disk image on sdcard with ``dd``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    # dd if=.otchado/build/precious/raspios.disk of=/dev/mmcblk0 \
        bs=8M status=progress oflag=sync


Boot your RaspberryPi_ with the new disk and check message of the day via ssh
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: shell-session

    $ ssh root@<your-rpi>
    pi@<your-rpi>'s password:
    Linux raspberrypi 5.10.17-v7+ #1403 SMP…
    === Hello world / 15:49:37 ===
    …

Default ``root`` password on the generated image is ``ertyuiop``


Inheriting from ``raspios`` recipe
==================================

The problem of the customization described in section above is that it is not automatic.
To make it fully automatic, a custom recipe based on ``raspios`` recipe must be done. The
recipe ``pidemo-motd.otr`` is an example of that and its entry-point is
``lib/python/otchado/recipes/demos/pidemo-motd/pidemo-motd.otr`` . Note that all recipes
provided by ``otchado`` are located in folder ``lib/python/otchado/recipes/`` and have ``.otr``
extension.


``pidemo-motd.otr`` recipe
~~~~~~~~~~~~~~~~~~~~~~~~~~

This recipe inherits from ``raspios``. It add a fourth task called ``customization``
which is placed just before task ``pack``. Note the relevant variable ``motd_content``
which defines the new content of ``/etc/motd``.
See ``pidemo-motd.otr`` below:

.. literalinclude:: ../lib/python/otchado/recipes/demos/pidemo-motd/pidemo-motd.otr
   :language: python


Update of the file ``/etc/motd`` using ``motd_content`` variable is done in ``Dockerfile-customization.jinja2``.
The ssh service is also enabled from this file.


.. literalinclude:: ../lib/python/otchado/recipes/demos/pidemo-motd/Dockerfile-customization.jinja2
   :language: jinja


Because of the inheritance, the tasks tree is similar to the ``raspios`` recipe

.. code-block:: shell-session

    (otchado) $ otchado run -q :pidemo-motd
    * tasks tree of recipe :pidemo-motd
    :pidemo-motd                     <otchado.task.RootTask>
    ├── :preprocessing               <otchado.tasks.template.Processor>
    ├── :bootstrap                   <otchado.tasks.raspios.Bootstrap>
    │   ├── :stage1                  <otchado.task.MethodTask>
    │   └── :stage2                  <otchado.tasks.docker.Build>
    ├── :customization               <otchado.tasks.docker.Build>
    └── :pack                        <otchado.tasks.raspios.Pack>
        ├── :mktars                  <otchado.task.MethodTask>
        ├── :gendisk                 <otchado.task.MethodTask>
        └── :keepdisk                <otchado.task.MethodTask>


The disk generation can be launched by the command below. It will run the four main tasks
in the right order (``preprocessing``, ``bootstrap``, ``customization``, and ``pack``):

.. code-block:: shell-session

    (otchado) $ otchado run :pidemo-motd -v
    …


The diagram below summarizes the work-flow of the recipe:

.. uml::

    @startuml
    skinparam rectangle<<**file**>> {
      backgroundColor LightBlue
    }
    skinparam rectangle<<**debian repository**>> {
      backgroundColor LightGreen
    }
    skinparam rectangle<<**docker image**>> {
      backgroundColor Pink
    }

    rectangle "raspios.otr" as rotr<<**file**>>
    rectangle "Dockerfile-bootstrap-stage2.jinja2" as dbst2<<**file**>>
    rectangle "Dockerfile-bootstrap-stage2" as dbst<<**file**>>
    rectangle "sources.list.jinja2" as srcl2<<**file**>>
    rectangle "sources.list" as srcl<<**file**>>
    rectangle "Dockerfile-customization.jinja2" as dcu2<<**file**>>
    rectangle "Dockerfile-customization" as dcu<<**file**>>

    rotr --> (preprocessing)
    dbst2 --> (preprocessing)
    srcl2 --> (preprocessing)
    dcu2 --> (preprocessing)
    (preprocessing) --> dbst
    (preprocessing) --> srcl
    (preprocessing) --> dcu

    rectangle "http://raspbian.raspberrypi.org/raspbian/" as http<<**debian repository**>>
    rectangle "pidemo-motd.disk" as disk<<**file**>>
    rectangle "otchado-pidemo-motd-initial" as imgi<<**docker image**>>

    rotr --> (bootstrap)
    dbst  --> (bootstrap)
    srcl  --> (bootstrap)
    http --> (bootstrap)
    (bootstrap) --> imgi

    rectangle "otchado-pidemo-motd-final" as imgf<<**docker image**>>

    rotr --> (customization)
    imgi --> (customization)
    dcu  --> (customization)
    (customization) --> imgf

    rotr --> (pack)
    imgf --> (pack)
    (pack) --> disk

    @enduml


After disk generation, some checks on docker side can be done. First on
"initial" image:

.. code-block:: shell-session

    (otchado) $ docker image ls
    REPOSITORY                    TAG       IMAGE ID         CREATED             SIZE
    …
    otchado-pidemo-motd-initial    latest    4128bd55be37     About a minute ago  1.09GB
    …
    (otchado) $ docker run -it otchado-pidemo-motd-initial cat /etc/motd
    …
    Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
    permitted by applicable law.


Then on "final" image:

.. code-block:: shell-session

    (otchado) $ docker image ls
    REPOSITORY                     TAG       IMAGE ID         CREATED             SIZE
    …
    otchado-pidemo-motd-final     latest    6708f3d99572     About a minute ago  1.09GB
    …
    (otchado) $ docker run -it otchado-pidemo-motd-final cat /etc/motd
    === Hello world ===
    
      * time: 17:09:00
      * host: valhaar
      * user: loghaan


Finally generated disk can be burned on sdcard:

.. code-block:: shell-session

    # dd if=.otchado/build/precious/pidemo-motd.disk of=/dev/mmcblk0 bs=8M \
        status=progress oflag=sync


and message of the day can be checked via ssh once your RaspberryPi_ has booted with the new sdcard:

.. code-block:: shell-session

    $ ssh pi@<your-rpi>
    pi@<your-rpi>'s password:
    Linux raspberrypi 5.10.17-v7+ #1403 SMP…
    === Hello world ===

      * time: 17:09:00
      * host: valhaar
      * user: loghaan
    …


``pidemo-hdmisrc.otr`` recipe
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This second example customizes the ``raspios`` distribution and allows your RaspberryPi_
to play in loop a list of video files. Video are fetched from ``youtube.com`` using the
tool ``youtube-dl``. This recipe works the same as the ``pidemo-motd.otr`` recipe described
in the previous section. Recipe files are stored in ``lib/python/otchado/recipes/demos/pidemo-hdmisrc/``.

See ``pidemo-hdmisrc.otr``:

.. literalinclude:: ../lib/python/otchado/recipes/demos/pidemo-hdmisrc/pidemo-hdmisrc.otr
   :language: python


And ``Dockerfile-customization.jinja2``:

.. literalinclude:: ../lib/python/otchado/recipes/demos/pidemo-hdmisrc/Dockerfile-customization.jinja2
   :language: jinja


About this document
===================

A PDF can be generated from this document ``README.rst`` with
the commands below:


.. code-block:: shell-session

    # apt-get install weasyprint plantuml sassc
    …
    (otchado) $ pip install -r requirements-dev.txt
    …
    (otchado) $ cd sphinx && make
    sphinx-build -M weasyprint . _build
    Running Sphinx v4.0.2
    loading translations [fr]... done
    loading pickled environment... done
    building [mo]: targets for 0 po files that are out of date
    building [weasyprint]: all documents
    updating environment: 0 added, 1 changed, 0 removed
    reading sources... [100%] index
    looking for now-outdated files... none found
    pickling environment... done
    checking consistency... done
    preparing documents... done
    assembling single document... done
    writing... done
    writing additional files... done
    copying static files... done
    copying extra files... done
    dumping object inventory... done
    build succeeded.

    The PDF file has been saved in _build/weasyprint.
