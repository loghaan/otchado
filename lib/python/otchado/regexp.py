import re
import fnmatch


def compile(pattern):
    if pattern.startswith('/') or pattern.endswith('/'):
        re_pattern = pattern[1:-1]
    else:
        re_pattern = fnmatch.translate(pattern)
    return re.compile(re_pattern)
