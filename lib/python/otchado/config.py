import os
import logging
from otchado import loghlp


logger = logging.getLogger(__name__)


class Options:
    def __init__(self,d):
        self.__dict__ = d

    def __repr__(self):
        return f'<Options {self.__dict__!r}>'


WORKDIR_BASENAME = '.otchado'
ITEMS_KEYS = ['workdir', 'metapaths']


_config_state = None

class Config:
    def __init__(self):
        global _config_state
        if _config_state is not None:
            self.__dict__ = _config_state
            return
        _config_state = self.__dict__
        self._options = {'log_level': 'info'}
        self._workdir = None
        self._scope = None

    def requireWorkdir(self):
        if self._workdir is not None:
            return
        pth = os.getcwd()
        while True:
            if pth=='/':
                wdir = f'/{WORKDIR_BASENAME}'
            else:
                wdir = f'{pth}/{WORKDIR_BASENAME}'
            if os.path.isdir(wdir):
                logger.debug(f'dir {wdir!r} exists')
                break
            logger.debug(f'dir {wdir!r} does not exist')
            if pth=='/':
                raise AssertionError(f'cannot find any {WORKDIR_BASENAME!r} workdir')
            pth = os.path.dirname(pth)
        self._workdir = wdir

    def initWorkdir(self):
        wdir = f'{os.getcwd()}/{WORKDIR_BASENAME}'
        if os.path.exists(wdir):
            raise AssertionError(f'{WORKDIR_BASENAME!r} workdir already exists')
        os.mkdir(wdir)
        os.mkdir(f'{wdir}/build')
        logger.debug(f'{wdir!r} workdir created')

    def requireScope(self):
        self.requireWorkdir()
        if self._scope is not None:
            return
        scope = {}
        scope['workdir'] = self._workdir
        metapaths = []
        metapaths.append(os.path.dirname(__file__) + '/recipes')
        metapaths.append(f'{self._workdir}/meta')
        scope['metapaths'] = metapaths
        conf_py = f'{self._workdir}/conf.py'
        if os.path.isfile(conf_py):
            content = open(conf_py).read()
            code = compile(content, conf_py, 'exec')
            exec(code, scope, scope)
            scope['workdir'] = self._workdir
        self._scope = scope

    def getMetaPaths(self):
        self.requireScope()
        return self._scope['metapaths']

    @property
    def scope(self):
        self.requireScope()
        return self._scope

    @property
    def items(self):
        self.requireScope()
        d = {}
        for k in ITEMS_KEYS:
            d[k] = self._scope[k]
        return d

    @property
    def options(self):
        return Options(self._options)

    def getBuildDir(self):
        self.requireWorkdir()
        return f'{self._workdir}/build'

    def parseArgs(self,parser,args):
        ns = parser.parse_args(args)
        for k,v in ns.__dict__.items():
            if v is None:
                continue
            self._options[k]=v
        logger.debug(self.options)
