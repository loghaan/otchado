import logging
import traceback
import sys
from otchado import config


INFO2  = logging.INFO - 2


def _(h):
    h = int(h,16)
    r = (h >> 16) & 0xff
    g = (h >> 8) & 0xff
    b = h & 0xff
    return f'\x1b[38;2;{r};{g};{b}m'


LEVEL_PROPERTIES = dict(
    critical = ( logging.CRITICAL, _("d00000"), _("d02020") ),
    error    = ( logging.ERROR,    _("d00000"), _("d02020") ),
    warning  = ( logging.WARNING,  _("d00000"), _("d02020") ),
    info     = ( logging.INFO,     _("4040ff"), _("8080ff") ),
    info2    = ( INFO2,            _("40c0c0"), _("80c0c0") ),
    debug    = ( logging.DEBUG,    _("f0f0f0"), _("808080") ),
)

COL_RESET = '\x1b[0m'


class Logger(logging.Logger):
    def __init__(self,name):
        logger = logging.getLogger(name)
        self.__dict__ = logger.__dict__

    def info2(self,*args,**kwargs):
        self.log(INFO2,*args,**kwargs)


def log_exception(logger, triplet=None, level=logging.ERROR):
    if triplet is None:
        et, ev, tb = sys.exc_info()
    else:
        et, ev, tb = triplet
    lines = traceback.format_exception(et, ev, tb)
    lines2 = []
    for ln in lines:
        lines2 += ln.split('\n')
    for ln in lines2:
        if ln=='':
            continue
        logger.log(level,ln)
    logger.log(level,'')


_stream_state = None

class Stream:
    def __init__(self):
        global _stream_state
        if _stream_state is not None:
            self.__dict__ = _stream_state
            return
        _stream_state = self.__dict__


def setup():
    conf = config.Config()
    level = LEVEL_PROPERTIES[conf.options.log_level][0]
    h = logging.StreamHandler(Stream())
    h.setFormatter(Formatter())
    logging.basicConfig(format='%(name)s: %(message)s', level=level)
    root_logger = logging.getLogger()
    for h in root_logger.handlers:
        h.setFormatter(Formatter())


class Formatter:
    def __init__(self):
        primary_fmt = '%s{name} %s{msg}%s'
        fmts = {}
        for k,v in LEVEL_PROPERTIES.items():
            lvl = v[0]
            fmts[lvl] = primary_fmt % (v[1],v[2],COL_RESET)
        fmts['default'] = primary_fmt % ('','','')
        self._formats = fmts

    def format(self,record):
        if record.levelno in self._formats:
            fmt = self._formats[record.levelno]
        else:
            fmt = self._formats['default']
        d = record.__dict__
        return fmt.format(**d)
