raise AssertionError

import inspect
import collections


NAME_FORMAT = '__otchado_ctx_{}'


def add_context(f_locals=None,**vars):
    if f_locals is None:
        frm = inspect.currentframe()
        frm = frm.f_back
        f_locals = frm.f_locals
    for k,v in vars.items():
        k = NAME_FORMAT.format(k)
        f_locals[k] = v


def collect_context(*args,stacked=False,maxdepth=30,want='tuple',defaults={}):
    if stacked:
        d = collections.defaultdict(list)
        def insert(k,v): d[k].insert(0,v)
    else:
        d = {}
        insert = d.__setitem__
    names = set()
    for k in defaults.keys():
        names.add(k)
    for a in args:
        names.add(a)
    frm = inspect.currentframe()
    for i in range(maxdepth):
        for n in names:
            k = NAME_FORMAT.format(n)
            if k in frm.f_locals:
                insert(n, frm.f_locals[k])
        frm = frm.f_back
        if frm is None:
            break
    for k,v in defaults.items():
        if k not in d:
            d[k] = v
    if want=='tuple':
        return tuple(d.values())
    elif want=='dict':
        return d
    else:
        raise ValueError
