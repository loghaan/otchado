import re
import binascii
from otchado import task, command, config
from otchado.tasks import docker
from otchado.utils import BinarySize


def parse_partitions_output(output):
    info = [None]
    sep = '\\s+'
    size = '(?P<size>[0-9]+B)'
    start = '(?P<start>[0-9]+B)'
    end = '(?P<end>[0-9]+B)'
    flags = '.*'
    type = '.*'
    fs = '.*'
    rx = f'^{sep} [1-9] {sep} {start} {sep} {end} {sep} {size} {sep} {type} {sep} {fs} {sep} {flags} $'
    rx = re.compile(rx, re.X)
    for ln in output.split('\n'):
        m = rx.match(ln)
        if not m:
            continue
        d = {}
        d['start'] = int(m.group('start')[:-1])
        d['end'] = int(m.group('end')[:-1])
        d['size'] = int(m.group('size')[:-1])
        info.append(d)
    return info


class Bootstrap(task.ClassTask):
    SUBNAMES = ['stage1']
    NAME = 'unpack'

    def stage1(self):
        conf = config.Config()
        bdir, rcp, suite, mirror = self.getFocusedVars('bdir','recipe','deb_suite','deb_mirror')
        arch, variant = self.getFocusedVars('deb_arch','deb_variant')
        with self.cmdCtx(f'{bdir}/recipes/{rcp.name}') as x:
            x.run(f'docker run %docker_workdir -t otchado-debootstrap --arch={arch} --variant={variant}'
                + f' --suite={suite} --mirror={mirror} -o bootstrap-stage1.tar.gz'
                + f' --create-overrides /etc/hosts,/etc/hostname'
                )

    def __init__(self):
        super().__init__()
        tsk = docker.Build(dockerfile='Dockerfile-bootstrap-stage2',fromimg='scratch',toimg=self.makeRef('initial_docker_image'))
        self.add(tsk,name='stage2')


class Pack(task.ClassTask):
    SUBNAMES = ['mktars','gendisk','keepdisk']
    NAME = 'pack'

    def mktars(self):
        bdir, final_docker_image, recipe = self.getFocusedVars('bdir','final_docker_image','recipe')
        with self.cmdCtx(f'{bdir}/recipes/{recipe.name}') as x:
            x.run('truncate --size 0 p1.tar')
            x.run('truncate --size 0 p2.tar')
            x.run('truncate --size 0 p2-overrides.tar')
            x.run(f'docker run -w /boot {final_docker_image} tar cv . >p1.tar')
            x.run(f'docker run -w / {final_docker_image} tar cv'
                + " --exclude='/.dockerenv'"
                + " --exclude='/otchado'"
                + " --exclude='/boot/*'"
                + " --exclude='/sys/*'"
                + " --exclude='/proc/*'"
                + " / >p2.tar")
            x.run(f'docker run -w /otchado/overrides {final_docker_image} tar cv . >p2-overrides.tar')

    def gendisk(self):
        bdir, disk_filename, recipe = self.getFocusedVars('bdir','disk_filename','recipe')
        disk_size, boot_part_start, boot_part_size, disk_uuid = self.getFocusedVars('disk_size', 'boot_part_start', 'boot_part_size', 'disk_uuid')
        boot_part_start = BinarySize(boot_part_start).value('s')
        boot_part_size = BinarySize(boot_part_size).value('s')
        with self.cmdCtx(f'{bdir}/recipes/{recipe.name}') as x:
            x.run(f'rm -f {disk_filename}')
            x.run(f'truncate --size {disk_size} {disk_filename}')
            parted = f'docker run %docker_workdir -t otchado-disktools parted -s /workdir/{disk_filename}'
            x.run(f'{parted} mklabel msdos')
            x.run(f'{parted} unit s mkpart p fat32 {boot_part_start} {boot_part_size}')
            x.run(f'{parted} set 1 boot on')
            x.run(f'{parted} unit s mkpart p ext4 {boot_part_start+boot_part_size} 100%')
            output = x.run(f'{parted} unit b print')
            parts = parse_partitions_output(output)
            x.createFile('mkfs-and-tarx.sh',f'''
                #!/bin/bash
                set -e
                touch /mnt/p1.loop
                fuseloop -O {parts[1]['start']} -S {parts[1]['size']} -w {disk_filename} /mnt/p1.loop
                mkfs.vfat -v /mnt/p1.loop
                cd /mnt/p1
                tar xvf /workdir/p1.tar
                mcopy -i /mnt/p1.loop -sv /mnt/p1/* ::
                cd /workdir
                touch /mnt/p2.loop
                fuseloop -O {parts[2]['start']} -S {parts[2]['size']} -w {disk_filename} /mnt/p2.loop
                mkfs.ext4 -v /mnt/p2.loop
                fuse2fs -o rw /mnt/p2.loop /mnt/p2
                cd /mnt/p2
                tar xvf /workdir/p2.tar
                tar xvf /workdir/p2-overrides.tar
            ''', mode='755')
            x.run(f'docker run %docker_workdir %docker_fuse -t otchado-disktools /workdir/mkfs-and-tarx.sh')

    def keepdisk(self):
        bdir, recipe = self.getFocusedVars('bdir','recipe')
        disk_filename, = self.getFocusedVars('disk_filename')
        with self.cmdCtx(f'{bdir}/recipes/{recipe.name}') as x:
            x.run(f'mv {disk_filename} ../../precious/{disk_filename}')
