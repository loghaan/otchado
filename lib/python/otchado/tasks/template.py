import jinja2
import os
import copy
from otchado import task, config, regexp


class Processor(task.CallableTask):
    def __init__(self,pattern='*.jinja2'):
        super().__init__()
        self._regexp = regexp.compile(pattern)
        self._extra_scopes = []

    def _copyFieldsFrom(self,tsk):
        super()._copyFieldsFrom(tsk)
        self._regexp = tsk._regexp
        self._extra_scopes = copy.copy(tsk._extra_scopes)

    def addExtraScope(self,pattern,**scope):
        rx = regexp.compile(pattern)
        self._extra_scopes += [(rx, scope)]

    def _getScope(self,f):
        rcp = self.getFocusedRecipe()
        final_scope = copy.copy(rcp.scope)
        for rx,scope in self._extra_scopes:
            if not rx.match(f):
                continue
            final_scope.update(scope)
        return final_scope

    def __call__(self):
        logger = self.logger
        rcp = self.getFocusedRecipe()
        bdir, = rcp.getVars('bdir')
        rbdir = f'{bdir}/recipes/{rcp.name}'
        loader = jinja2.FileSystemLoader(rbdir)
        env = jinja2.Environment(loader=loader)
        for f in rcp.files:
            if not self._regexp.match(f):
                continue
            bf,ext = os.path.splitext(f)
            fn_out = f'{rbdir}/{bf}'
            logger.info(f'process template {f!r}')
            t = env.get_template(f)
            scope = self._getScope(f)
            content = t.render(**scope)
            fh = open(fn_out,'w')
            fh.write(content)
            fh.close()
