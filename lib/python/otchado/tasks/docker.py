from otchado import task, command, config
from otchado.recipe import maybe_deref


class Build(task.CallableTask):
    def __init__(self,*,dockerfile='',fromimg,toimg):
        super().__init__()
        self._dockerfile = dockerfile
        self._fromimg = fromimg
        self._toimg = toimg

    def __call__(self):
        bdir,rcp = self.getFocusedVars('bdir','recipe')
        fromimg = maybe_deref(self._fromimg)
        toimg = maybe_deref(self._toimg)
        with self.cmdCtx(f'{bdir}/recipes/{rcp.name}') as x:
            x.run(f'docker build %docker_cache . -t {toimg} --build-arg OTCHADO_FROMIMG={fromimg} -f {self._dockerfile}')
