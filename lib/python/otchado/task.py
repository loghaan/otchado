import os
import logging
import copy
import inspect
from otchado import loghlp, config, command, recipe


SUCCESS = None
FAILURE = -1


class Task:
    NAME = None

    def __init__(self):
        klass = self.__class__
        self._name = klass.NAME
        self._parent = None

    def _getIndexAndSize(self):
        ptsk = self._parent
        if not issubclass(ptsk.__class__,CompoundTask):
            return 0,1
        size = len(ptsk._subnames)
        idx = ptsk._subnames.index(self.name)
        return idx,size

    def isFirst(self):
        idx,size = self._getIndexAndSize()
        return idx==0

    def isLast(self):
        idx,size = self._getIndexAndSize()
        return idx==(size-1)

    @classmethod
    def getFocusedRecipe(cls):
        k = recipe.FOCUSED_RECIPE_KEY
        frm = inspect.currentframe()
        for i in range(30):
            if frm is None:
                break
            if k in frm.f_locals:
                return frm.f_locals[k]
            frm = frm.f_back
        raise AssertionError

    def getFocusedVars(self,*names):
        rcp = self.getFocusedRecipe()
        return rcp.getVars(*names)

    def makeRef(self,name):
        rcp = self.getFocusedRecipe()
        return rcp.makeRef(name)

    def cmdCtx(self,*args,**kwargs):
        return command.Context(*args,logger=self.logger,**kwargs)

    @property
    def parent(self):
        return self._parent

    @property
    def name(self):
        return self._name

    @property
    def track(self):
        tsk = self
        l = []
        while tsk:
            n = tsk.name
            if n is None:
                n = '<noname>'
            l.append(n)
            tsk = tsk.parent
        l.reverse()
        l.insert(0,'')
        trk = ':'.join(l)
        return trk

    @property
    def logger(self):
        return loghlp.Logger(self.track)

    def run(self):
        logger = self.logger
        try:
            return self._run()
        except:
            loghlp.log_exception(logger)
            return FAILURE

    def _run(self):
        raise NotImplementedError('_run() not implemented')

    def walk(self):
        yield self


class CompoundTask(Task):
    def __init__(self):
        super().__init__()
        self._subnames = []
        self._subtasks = {}
        self._current_subtask = None

    @property
    def track(self):
        trk = super().track
        if self._current_subtask is None:
            return trk
        trk += f':{self._current_subtask.name}'
        return trk

    def add(self,obj,*,name=None,after=None,before=None):
        if issubclass(obj.__class__,Task):
            tsk = obj
        elif callable(obj):
            tsk = CallableTask(obj)
        else:
            raise TypeError
        tsk._parent = self
        if name is not None:
            tsk._name = name
        def make_idx(name_or_idx):
            if isinstance(name_or_idx,int):
                return name_or_idx
            else:
                return self._subnames.index(name_or_idx)
        if after is not None:
            idx = make_idx(after) + 1
        elif before is not None:
            idx = make_idx(before)
        else:
            idx = len(self._subnames)
        if idx < 0:
            raise AssertionError
        if tsk.name in self._subtasks:
            raise AssertionError
        self._subtasks[tsk.name]=tsk
        self._subnames.insert(idx,tsk.name)

    def remove(self,name):
        if name not in self._subtasks:
            raise KeyError(name)
        idx = self._subnames.index(name)
        if idx < 0:
            raise AssertionError
        self._subnames.pop(idx)
        del self._subtasks[name]

    def __add__(self,obj):
        self.add(obj)
        return self

    def __radd__(self,obj):
        self.add(obj,before=0)
        return self

    def __setitem__(self,name,obj):
        self.add(obj,name=name)

    def __getitem__(self,name):
        return self._subtasks[name]

    def __delitem__(self,name):
        self.remove(name)

    def _run(self):
        for n in self._subnames:
            tsk = self._subtasks[n]
            self._current_subtask = tsk
            try:
                failure = tsk.run()
            finally:
                self._current_subtask = None
            if failure:
                return failure

    def walk(self):
        yield self
        for n in self._subnames:
            tsk = self._subtasks[n]
            yield from tsk.walk()


class ClassTask(CompoundTask):
    def __init__(self):
        super().__init__()
        klass = self.__class__
        for n in klass.SUBNAMES:
            clbl = getattr(klass,n)
            tsk = MethodTask(clbl)
            self.add(tsk,name=n)


class CallableTask(Task):
    def __init__(self,clbl=None):
        super().__init__()
        if clbl is None:
            self._callable = self.__call__
        else:
            self._callable = clbl
        if self._name is None:
            self._name = self._callable.__name__

    def _run(self):
        f = self._callable
        f()


class MethodTask(CallableTask):
    def _run(self):
        f = self._callable
        f(self._parent)


class RootTask(CompoundTask):
    def __init__(self,name):
        super().__init__()
        self._name = name
