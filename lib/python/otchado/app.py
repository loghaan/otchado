import logging
import sys
import argparse
import re
import os
import copy
import pprint
from otchado import recipe, loghlp, config, task


logger = logging.getLogger(__name__)


ACTIONS = {}
RECIPE_EXTENSION = '.otr'
EXTENSION_LENGTH = len(RECIPE_EXTENSION)


class LogLevelAppender:
    def __init__(self):
        self._level = None

    def append(self,level):
        self._level = level.lower()

    def getValue(self):
        return self._level


class ArgumentParser(argparse.ArgumentParser):
    def __init__(self):
        actions = '|'.join(ACTIONS)
        super().__init__(usage=f'%(prog)s <{actions}> [optional arguments]')
        log_level_appender = LogLevelAppender()
        self.add_argument('--log-level', default=log_level_appender, action='append', metavar='LEVEL', help='sset log level to LEVEL')
        self.add_argument('--no-docker-cache', dest='use_docker_cache', default=True, action='store_false', help='disable docker cache')
        self.add_argument('--reset', dest='reset', default=False, action='store_true', help='reset recipe build dir')
        self.add_argument('-v', dest='log_level', default=log_level_appender, action='append_const', const='info2', help="set log level to 'info2'")
        self.add_argument('-q', dest='log_level', default=log_level_appender, action='append_const', const='error', help="set log level to 'error'")

    def parse_args(self, args):
        prog = args[0]
        ns,args = super().parse_known_args(args[1:])
        ns.prog = prog
        ns.tracks = []
        ns.action = None
        for a in args:
            if a.startswith(':'):
                ns.tracks.append(a)
            else:
                if a not in ACTIONS:
                    self.error(f'invalid action {a!r}')
                if ns.action is not None:
                    self.error('more than one action specified')
                ns.action = a
        if ns.action is None:
            self.error('no action secified')
        for k in 'log_level',:
            v = getattr(ns,k).getValue()
            setattr(ns,k,v)
        return ns


class Scanner:
    def __init__(self):
        self._recipes = {}

    @property
    def recipes(self):
        return copy.copy(self._recipes)

    def scan(self):
        self._depth = 0
        conf = config.Config()
        for e in conf.getMetaPaths():
            if os.path.isdir(e):
                self._scanDir(e)

    def _scanDir(self,dn):
        if self._depth > 10:
            return
        self._depth += 1
        rcp_filenames = []
        dirnames = []
        name = None
        for bn in os.listdir(dn):
            pth = f'{dn}/{bn}'
            if bn.endswith(RECIPE_EXTENSION):
                rcp_filenames.append(bn)
            elif os.path.isdir(pth):
                dirnames.append(pth)
        if len(rcp_filenames)==0:
            for pth in dirnames:
                self._scanDir(pth)
        else:
            for bn in rcp_filenames:
                name = f'{bn[:-EXTENSION_LENGTH]}'
                recipe.Recipe(name,f'{dn}/{bn}',all_recipes=self._recipes)
        self._depth -= 1


def action(name):
    def inner(obj):
        ACTIONS[name]=obj
    return inner


class Application:
    def __init__(self):
        self._scanner = Scanner()
        self._tasks = {}

    def __call__(self):
        parser = ArgumentParser()
        conf = config.Config()
        conf.parseArgs(parser, sys.argv)
        loghlp.setup()
        f = ACTIONS[conf.options.action]
        f(self)

    @property
    def recipes(self):
        return self._scanner.recipes

    @action('init')
    def init(self):
        conf = config.Config()
        conf.initWorkdir()

    @action('show-config')
    def showConf(self):
        conf = config.Config()
        print('* config items')
        pprint.pprint(conf.items)

    @action('show-recipes')
    def showRecipes(self):
        self.scan()
        print('* recipes list')
        for tr in sorted(self.recipes.keys()):
            print(tr)
        print()

    @action('show-tasks')
    def showTasks(self):
        conf = config.Config()
        self.scan()
        for tr in conf.options.tracks:
            rtr = self.getRootTrack(tr)
            self.loadTasks(rtr)
        first_tree = True
        for ttr,tsk in self._tasks.items():
            ttrl = ttr.split(':')
            ttrl.pop(0)
            size = len(ttrl)
            tree_ln = ''
            if size==1 and tsk.isFirst():
                closed_indexes = set()
                if not first_tree:
                    print()
                print(f'* tasks tree of recipe {ttr}')
                first_tree = False
            for idx in range(size):
                if idx==(size-1):
                    tree_ln += f':{ttrl[-1]}'
                elif idx==(size-2):
                    if tsk.isLast():
                        tree_ln += '└── '
                        closed_indexes.add(idx)
                    else:
                        tree_ln += '├── '
                else:
                    if idx in closed_indexes:
                        tree_ln += '    '
                    else:
                        tree_ln += '│   '
            mod = tsk.__class__.__module__
            klass = tsk.__class__.__qualname__
            print(f'{tree_ln:32} <{mod}.{klass}>')

    @action('run')
    def run(self):
        conf = config.Config()
        self.scan()
        for tr in conf.options.tracks:
            rtr = self.getRootTrack(tr)
            self.loadTasks(rtr)
        for tr in conf.options.tracks:
            rtr = self.getRootTrack(tr)
            rcp = self.recipes[rtr]
            tsk = self._tasks[tr]
            rcp.focusRecipe(locals())
            tsk.run()
            rcp.unfocusRecipe(locals())

    def scan(self):
        self._scanner.scan()

    def loadTasks(self,track):
        rcp = self.recipes[track]
        rcp.load()
        tasks = rcp._scope['tasks']
        for tsk in tasks.walk():
            tr = tsk.track
            if tr not in self._tasks:
                self._tasks[tr] = tsk

    def getRootTrack(self,track):
        if track[0] != ':':
            raise AssertionError
        idx = track[1:].find(':')
        if idx >= 0:
            return track[:idx+1]
        else:
            return track


def main():
    app = Application()
    app()
