import os
import subprocess
import logging
import textwrap
import shutil
import shlex
import asyncio
import re
import functools
from otchado import loghlp, config


class Reader:
    def __init__(self,executor,process,*,loop,blocksize=None,separator=None):
        self._executor = executor
        self._logger = executor._logger
        self._process = process
        self._output = b''
        stdout = process.stdout
        if (blocksize is None) and (separator is None):
            self._readChunk = stdout.readline
            self._logChunk = self._logLine
        elif isinstance(blocksize,int):
            self._readChunk = functools.partial(stdout.read,blocksize)
            self._logcCunk = self._logBytes
        elif isinstance(separator,bytes):
            self._readChunk = functools.partial(stdout.readuntil,separator)
            self._logChunk = functools.partial(self._logBytes,separator=separator)
        else:
            raise AssertionError
        task = loop.create_task(self.__call__())
        task.add_done_callback(self._done)
        self._task = task

    @property
    def output(self):
        return self._output

    def _logBytes(self,chunk,*,separator=None):
        logger = self._logger
        if separator is not None:
            if chunk.endswith(separator):
                chunk = chunk[:-1]
        chunk = repr(chunk)
        chunk = chunk[2:-1]
        logger.info2(chunk)

    def _logLine(self,chunk):
        logger = self._logger
        if chunk.endswith(b'\n'):
            chunk = chunk[:-1]
        self._logger.info2(chunk.decode())

    async def __call__(self):
        while True:
            try:
                chunk = await self._readChunk()
            except asyncio.exceptions.IncompleteReadError:
                await self._process.stdout.read()
                break
            if len(chunk)==0:
                break
            self._output += chunk
            self._logChunk(chunk)

    def _done(self,tsk):
        logger = self._logger
        try:
            tsk.result()
        except:
            loghlp.log_exception(logger)


class CommandFailed(Exception):
    pass


class Context:
    def __init__(self,workdir,*,logger=None):
        self._workdir = workdir
        if logger is None:
            self._logger = logging.getLogger(__name__)
        else:
            self._logger = logger

    def createFile(self,path,content,*,dedent=True,mode=None):
        logger = self._logger
        if dedent:
            if content[0]=='\n':
                content=content[1:]
            content = textwrap.dedent(content)
        logger.info(f'create file {path!r}')
        for ln in content.split('\n'):
            logger.info(f'..  {ln}')
        f = self.open(path,'w')
        f.write(content)
        f.close()
        if mode is not None:
            self.run(f'chmod {mode} {path}')

    def pathExists(self,path):
        return os.path.exists(f'{self._workdir}/{path}')

    def open(self,path,mode):
        return open(f'{self._workdir}/{path}',mode)

    def evaluateMacro(self,text):
        text_out = ''
        for token in re.split('(%[A-Za-z_][A-Za-z0-9_]*)',text):
            if not token.startswith('%'):
                text_out += token
                continue
            attrname = f'_macro_{token[1:]}'
            if not hasattr(self,attrname):
                text_out += token
                continue
            f = getattr(self,attrname)
            text_out += f()
        return text_out

    def _macro_docker_cache(self):
        conf = config.Config()
        if conf.options.use_docker_cache:
            return ''
        else:
            return '--no-cache'

    def _macro_docker_user(self):
        return f'--user {os.getuid()}:{os.getgid()}'

    def _macro_docker_workdir(self):
        if self._workdir is None:
            return ''
        else:
            return f'-v {self._workdir}:/workdir -w /workdir'

    def _macro_docker_fuse(self):
        return '--device /dev/fuse --cap-add SYS_ADMIN --security-opt apparmor:unconfined'

    def run(self,cmd,*,blocksize=None,separator=None):
        logger = self._logger
        cmd = self.evaluateMacro(cmd)
        logger.info(f'run {cmd!r}')
        kwargs = {}
        if self._workdir is not None:
            kwargs['cwd'] = self._workdir
        kwargs['stdout']=subprocess.PIPE
        kwargs['stderr']=subprocess.STDOUT
        kwargs['stdin']=subprocess.PIPE
        loop = asyncio.get_event_loop()
        u = loop.run_until_complete
        p = u(asyncio.create_subprocess_shell(cmd,**kwargs))
        rdr = Reader(self,p,loop=loop,blocksize=blocksize,separator=separator)
        rcode = u( p.wait() )
        if rcode != 0:
            args = shlex.split(cmd)
            if shutil.which(args[0]) is None:
                logger.error(f'it seems that {args[0]!r} is not in your $PATH !!!')
            raise CommandFailed(f"{cmd!r} exits with status {p.returncode}")
        return rdr.output.decode()

    def __enter__(self):
        logger = self._logger
        logger.info(f'entering {self._workdir!r}')
        return self

    def __exit__(self,*args):
        logger = self._logger
        logger.info(f'leaving {self._workdir!r}')
