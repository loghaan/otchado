#!/usr/bin/env python3

import sys
import subprocess
import copy
import argparse
import tempfile
import os


class ArgumentParser(argparse.ArgumentParser):
    def __init__(self):
        super().__init__()
        self.add_argument('--variant',           default=None)
        self.add_argument('--arch',              default=None)
        self.add_argument('--suite',             default=None)
        self.add_argument('-o', '--output',      default=None)
        self.add_argument('--mirror',            default=None)
        self.add_argument('--create-overrides',  default=None)

    def parse_args(self, args):
        ns = super().parse_args(args[1:])
        ns.prog = os.path.basename(args[0])
        return ns


def subprocess_run(cmd,*,cwd=None,title):
    s_cmd = ' '.join(cmd)
    print(f"{title}: {s_cmd}")
    cp = subprocess.run(cmd,shell=False,cwd=cwd)
    if cp.returncode != 0:
        raise AssertionError(f'{s_cmd!r} returns {cp.returncode}')


def run_real_debootstrap(ns,target):
    cmd = ['debootstrap']
    if ns.arch is not None:
        cmd += [f'--arch={ns.arch}']
    cmd += ['--foreign']
    if ns.variant is not None:
        cmd += [f'--variant={ns.variant}']
    cmd += ['--no-check-gpg']
    cmd += [f'{ns.suite}']
    cmd += [f'{target}']
    cmd += [f'{ns.mirror}']
    subprocess_run(cmd,title=ns.prog)


def run_tar(ns,target):
    cmd = ['tar']
    flags = 'cvf'
    if ns.output.startswith('/'):
        raise AssertionError
    if ns.output.endswith('.tar.gz'):
        flags = 'z' + flags
    elif ns.output.endswith('.tar'):
        pass
    else:
        raise AssertionError
    cmd += [flags]
    cmd += [f'/workdir/{ns.output}']
    cmd += ['.']
    subprocess_run(cmd,cwd=target,title=ns.prog)


def main():
    p = ArgumentParser()
    ns = p.parse_args(sys.argv)
    target = tempfile.mkdtemp()
    run_real_debootstrap(ns,target)
    run_tar(ns,target)


if __name__=='__main__':
    main()
