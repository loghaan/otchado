import os
import copy


class BinarySize:
    UNITS = [
        (1,     ('b',)),
        (512,   ('s',)),
        (1024,  ('k', 'K', 'KiB')),
        (1000,  ('KB',)),
        (2**20, ('M','MiB')),
        (10**6, ('MB',)),
        (2**30, ('G', 'GiB',)),
        (10**9, ('GB',)),
    ]

    MULTIPLIERS = {}
    for m,suffs in UNITS:
        for s in suffs:
            MULTIPLIERS[s] = m

    DEFAULT_SUFFIX = 'b'

    def __init__(self, size, suffix=None):
        if isinstance(size,int):
            size = str(size)
        multipliers = copy.copy(self.MULTIPLIERS)
        if suffix is not None:
            multipliers[''] = multipliers[suffix]
            self._suffix = suffix
        else:
            self._suffix = self.DEFAULT_SUFFIX
        for s,m in multipliers.items():
           if size.endswith(s):
                end = -len(s)
                if end==0:
                    end=None
                self._numbytes = int(size[:end]) * m
                return
        raise ValueError

    def value(self,suffix=None):
        if suffix is None:
            suffix = self.DEFAULT_SUFFIX
        m = self.MULTIPLIERS[suffix]
        modulo = self._numbytes % m
        if modulo != 0:
            raise ValueError('not a multiple of {m}')
        return int(self._numbytes / m)

    def __repr__(self):
        return repr(self._numbytes)


def ensure_subdirs(path, *names):
    for n in names:
        p = f'{path}/{n}'
        if not os.path.exists(p):
            os.mkdir(p)
