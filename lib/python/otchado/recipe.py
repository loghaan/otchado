import os
import copy
import inspect
from otchado import loghlp, config, task, utils, command


logger = loghlp.Logger(__name__)


FOCUSED_RECIPE_KEY = '__otchado_focused_recipe'


class Ref:
    def __init__(self,recipe,name):
        self._recipe = recipe
        self._name = name

    def __call__(self):
        scope = self._recipe.scope
        return scope[self._name]


def maybe_deref(obj):
    if isinstance(obj,Ref):
        return obj()
    else:
        return obj

class Recipe:
    def __init__(self,name,path,*,all_recipes):
        self._name = name
        self._path = path
        self._scope = None
        self._all_recipes = all_recipes
        self._base_recipes = set()
        self._files = {}
        self._load = False
        self._all_recipes = all_recipes
        if self.track in all_recipes:
            logger.error(f'recipe name collision {name!r}')
            raise AssertionError
        all_recipes[self.track] = self

    def focusRecipe(self,f_locals):
        f_locals[FOCUSED_RECIPE_KEY] = self

    def unfocusRecipe(self,f_locals):
        del f_locals[FOCUSED_RECIPE_KEY]

    @property
    def name(self):
        return self._name

    @property
    def track(self):
        return f':{self._name}'

    @property
    def logger(self):
        return loghlp.Logger(self.track)

    def load(self):
        if self._load:
            return
        self.readScope()
        self.indexFiles()
        self.prepareBuildDir()
        self._load = True

    @property
    def files(self):
        return self._files.keys()

    @property
    def scope(self):
        return copy.copy(self._scope)

    def inheritsFrom(self,*args):
        tasks = self._scope['tasks']
        all_recipes = self._all_recipes
        for name in args:
            track = f':{name}'
            rcp = all_recipes[track]
            content = open(rcp._path).read()
            code = compile(content, rcp._path, 'exec')
            exec(code,self._scope,self._scope)
            self._base_recipes.update(rcp._base_recipes)
            self._base_recipes.add(rcp)

    def readScope(self):
        if self._scope is not None:
            return
        conf = config.Config()
        self._scope = {}
        scope = self._scope
        scope['ref'] = self.makeRef
        scope['bdir'] = conf.getBuildDir()
        scope['inherits_from'] = self.inheritsFrom
        scope['tasks'] = task.RootTask(self._name)
        scope['recipe'] = self
        scope[FOCUSED_RECIPE_KEY] = self
        content = open(self._path).read()
        code = compile(content, self._path, 'exec')
        exec(code, scope, scope)

    def makeRef(self,name):
        return Ref(self,name)

    def getVars(self,*args):
        rval = []
        for k in args:
            v = self._scope[k]
            rval.append(v)
        return tuple(rval)

    def indexFiles(self):
        recipes = list(self._base_recipes) + [self]
        self._files = {}
        for r in recipes:
            topdir = os.path.dirname(r._path)
            l = len(topdir)
            for root,dirs,files in os.walk(topdir):
                for fn in files:
                    k = f'{root[l:]}/{fn}'
                    if k.startswith('/'):
                        k = k[1:]
                    v = f'{root}/{fn}'
                    if k in self._files:
                        raise AssertionError(f'file conflict {k!r}')
                    self._files[k] = v

    def prepareBuildDir(self):
        conf = config.Config()
        bdir, = self.getVars('bdir')
        if not os.path.exists(bdir):
            try:
                os.mkdir(bdir)
            except:
                raise AssertionError(f'cannot create {bdir!r}')
        utils.ensure_subdirs(bdir, 'downloads', 'recipes', 'precious')
        with command.Context(f'{bdir}/recipes',logger=self.logger) as x:
            if conf.options.reset:
                x.run(f'rm -fr {self._name}')
                x.run(f'mkdir {self._name}')
            elif not x.pathExists(self._name):
                x.run(f'mkdir {self._name}')
        rbdir = f'{bdir}/recipes/{self._name}'
        with command.Context(rbdir,logger=self.logger) as x:
            dirs = set()
            for k,v in self._files.items():
                d = os.path.dirname(k)
                if d=='':
                    continue
                dirs.add(d)
            for d in dirs:
                x.run(f'mkdir -p {d}')
            for k,v in self._files.items():
                x.run(f'cp {v} {k}')
