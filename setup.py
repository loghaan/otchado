from setuptools import setup, find_packages


def parse_requirements(requirements):
    with open(requirements) as f:
        return [l.strip('\n') for l in f if l.strip('\n') and not l.startswith('#')]


setup(
    name='otchado',
    version='0.0.1',
    description='Build raspberry disks derivated from raspios using docker and qemu',
    author='loghaan',
    author_email='loghaan@benead.org',
    install_requires=parse_requirements('requirements.txt'),
    package_dir={'': 'lib/python'},
    packages=find_packages(where='lib/python'),
    include_package_data=True,
    python_requires='>=3.8, <4',
    entry_points={
        'console_scripts': [
            'otchado=otchado.app:main',
        ],
    },
)
